from django.db import models
from django.shortcuts import reverse


class Post(models.Model):
    title = models.CharField(max_length=150, db_index=True)
    slug = models.SlugField(max_length=150, unique=True)
    tags = models.ManyToManyField('Tag', blank=True, related_name='posts', default=None)
    body = models.TextField(blank=True, db_index=True)
    date_pub = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('detail_url', kwargs={'slug': self.slug})

    def __str__(self):
        return "{}".format(self.title)


class Tag(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)

    def __str__(self):
        return '{}'.format(self.title)

    def get_absolute_url(self):
        return reverse('tags', kwargs={'slug': self.slug})


class Teacher(models.Model):
    name = models.CharField(max_length=50, verbose_name=u"Ім'я")
    surname = models.CharField(max_length=50, verbose_name=u"Прізвище")
    last_name = models.CharField(max_length=50, verbose_name=u"По батькові")
    name_subject = models.CharField(max_length=50, verbose_name=u"Назва предмета")
    default_room = models.CharField(max_length=50, verbose_name=u"Номер кабінету")

    def __str__(self):
        return '{} {} {}, предмет - {}, кабінет - {}'.format(self.surname,
                                                             self.name,
                                                             self.last_name,
                                                             self.name_subject,
                                                             self.default_room
                                                             )


class Group(models.Model):
    number = models.CharField(max_length=50, verbose_name=u"Номер групи")

    def __str__(self):
        return str(self.number)


class Monday(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)

    first_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="first_teacher",
                                     default=None, blank=True, verbose_name=u"Перша пара")
    first_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для першої пари")

    second_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="second_teacher",
                                      default=None, blank=True, verbose_name=u"Друга пара")
    second_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для другої пари")

    third_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="third_teacher",
                                     default=None, blank=True, verbose_name=u"Третя пара")
    third_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для третьої пари")

    fourth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fourth_Teacher",
                                      default=None, blank=True, verbose_name=u"Четверта пара")
    fourth_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для четвертої пари")

    fifth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fifth_Teacher",
                                     default=None, blank=True, verbose_name=u"П'ята пара")
    fifth_lesson_room = models.CharField(max_length=50, default="", blank=True,verbose_name=u"Аудиторія для п'ятої пари")

    class Meta:
        ordering = ('-group',)

    def save(self, *args, **kwargs):
        if self.first_lesson is not None and self.first_lesson_room == "":
            self.first_lesson_room = self.first_lesson.default_room

        if self.second_lesson is not None and self.second_lesson_room == "":
            self.second_lesson_room = self.second_lesson.default_room

        if self.third_lesson is not None and self.third_lesson_room == "":
            self.third_lesson_room = self.third_lesson.default_room

        if self.fourth_lesson is not None and self.fourth_lesson_room == "":
            self.fourth_lesson_room = self.fourth_lesson.default_room

        if self.fifth_lesson is not None and self.fifth_lesson_room == "":
            self.fifth_lesson_room = self.fifth_lesson.default_room

        super(Monday, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(str(self.group.number))


class Tuesday(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)

    first_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="w_first_teacher",
                                     default=None, blank=True, verbose_name=u"Перша пара")
    first_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для першої пари")

    second_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="w_second_teacher",
                                      default=None, blank=True, verbose_name=u"Друга пара")
    second_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для другої пари")

    third_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="w_third_teacher",
                                     default=None, blank=True, verbose_name=u"Третя пара")
    third_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для третьої пари")

    fourth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="w_fourth_Teacher",
                                      default=None, blank=True, verbose_name=u"Четверта пара")
    fourth_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для четвертої пари")

    fifth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="w_fifth_Teacher",
                                     default=None, blank=True, verbose_name=u"П'ята пара")
    fifth_lesson_room = models.CharField(max_length=50, default="", blank=True, verbose_name=u"Аудиторія для п'ятої пари")

    class Meta:
        ordering = ('-group',)

    def save(self, *args, **kwargs):
        if self.first_lesson is not None and self.first_lesson_room == "":
            self.first_lesson_room = self.first_lesson.default_room

        if self.second_lesson is not None and self.second_lesson_room == "":
            self.second_lesson_room = self.second_lesson.default_room

        if self.third_lesson is not None and self.third_lesson_room == "":
            self.third_lesson_room = self.third_lesson.default_room

        if self.fourth_lesson is not None and self.fourth_lesson_room == "":
            self.fourth_lesson_room = self.fourth_lesson.default_room

        if self.fifth_lesson is not None and self.fifth_lesson_room == "":
            self.fifth_lesson_room = self.fifth_lesson.default_room

        super(Tuesday, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(str(self.group.number))

class Wednesday(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)

    first_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="t_first_teacher",
                                     default=None, blank=True)
    first_lesson_room = models.CharField(max_length=50, default="", blank=True)

    second_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="t_second_teacher",
                                      default=None, blank=True)
    second_lesson_room = models.CharField(max_length=50, default="", blank=True)

    third_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="t_third_teacher",
                                     default=None, blank=True)
    third_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fourth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="t_fourth_Teacher",
                                      default=None, blank=True)
    fourth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fifth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="t_fifth_Teacher",
                                     default=None, blank=True)
    fifth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    class Meta:
        ordering = ('-group',)

    def save(self, *args, **kwargs):
        if self.first_lesson is not None and self.first_lesson_room == "":
            self.first_lesson_room = self.first_lesson.default_room

        if self.second_lesson is not None and self.second_lesson_room == "":
            self.second_lesson_room = self.second_lesson.default_room

        if self.third_lesson is not None and self.third_lesson_room == "":
            self.third_lesson_room = self.third_lesson.default_room

        if self.fourth_lesson is not None and self.fourth_lesson_room == "":
            self.fourth_lesson_room = self.fourth_lesson.default_room

        if self.fifth_lesson is not None and self.fifth_lesson_room == "":
            self.fifth_lesson_room = self.fifth_lesson.default_room

        super(Wednesday, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(str(self.group.number))

class Thursday(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)

    first_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="th_first_teacher",
                                     default=None, blank=True)
    first_lesson_room = models.CharField(max_length=50, default="", blank=True)

    second_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="th_second_teacher",
                                      default=None, blank=True)
    second_lesson_room = models.CharField(max_length=50, default="", blank=True)

    third_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="th_third_teacher",
                                     default=None, blank=True)
    third_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fourth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="th_fourth_Teacher",
                                      default=None, blank=True)
    fourth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fifth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="th_fifth_Teacher",
                                     default=None, blank=True)
    fifth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    class Meta:
        ordering = ('-group',)

    def save(self, *args, **kwargs):
        if self.first_lesson is not None and self.first_lesson_room == "":
            self.first_lesson_room = self.first_lesson.default_room

        if self.second_lesson is not None and self.second_lesson_room == "":
            self.second_lesson_room = self.second_lesson.default_room

        if self.third_lesson is not None and self.third_lesson_room == "":
            self.third_lesson_room = self.third_lesson.default_room

        if self.fourth_lesson is not None and self.fourth_lesson_room == "":
            self.fourth_lesson_room = self.fourth_lesson.default_room

        if self.fifth_lesson is not None and self.fifth_lesson_room == "":
            self.fifth_lesson_room = self.fifth_lesson.default_room

        super(Thursday, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(str(self.group.number))

class Friday(models.Model):
    group = models.ForeignKey(Group, null=True, on_delete=models.SET_NULL)

    first_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fr_first_teacher",
                                     default=None, blank=True)
    first_lesson_room = models.CharField(max_length=50, default="", blank=True)

    second_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fr_second_teacher",
                                      default=None, blank=True)
    second_lesson_room = models.CharField(max_length=50, default="", blank=True)

    third_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fr_third_teacher",
                                     default=None, blank=True)
    third_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fourth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fr_fourth_Teacher",
                                      default=None, blank=True)
    fourth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    fifth_lesson = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL, related_name="fr_fifth_Teacher",
                                     default=None, blank=True)
    fifth_lesson_room = models.CharField(max_length=50, default="", blank=True)

    class Meta:
        ordering = ('-group',)

    def save(self, *args, **kwargs):
        if self.first_lesson is not None and self.first_lesson_room == "":
            self.first_lesson_room = self.first_lesson.default_room

        if self.second_lesson is not None and self.second_lesson_room == "":
            self.second_lesson_room = self.second_lesson.default_room

        if self.third_lesson is not None and self.third_lesson_room == "":
            self.third_lesson_room = self.third_lesson.default_room

        if self.fourth_lesson is not None and self.fourth_lesson_room == "":
            self.fourth_lesson_room = self.fourth_lesson.default_room

        if self.fifth_lesson is not None and self.fifth_lesson_room == "":
            self.fifth_lesson_room = self.fifth_lesson.default_room

        super(Friday, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(str(self.group.number))
from django.urls import path
from .views import (
    index,
    PostDetail,
    TagDetail,
    SubjectDetail,
    SubjectIndex
)

urlpatterns = [
    path("", SubjectIndex.as_view(), name="index"),
    path("<str:slug>/", SubjectDetail.as_view(), name="detail_url"),
    #path("tags/<str:slug>/", TagDetail.as_view(), name="tags"),
    #path("<str:slug>/", PostDetail.as_view(), name="detail_url")

]

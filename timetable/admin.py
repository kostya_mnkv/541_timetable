from django.contrib import admin
from .models import Post, Tag, Group, Teacher, Monday, Tuesday, Wednesday, Thursday, Friday


# Register your models here.
#admin.site.register(Post)
#admin.site.register(Tag)

class TeacherAdmin(admin.ModelAdmin):
    # Sorting field
    ordering = ("default_room",)
    # Grouping fields
    fieldsets = (
        ("П.І.П", {
            'fields': ('surname', 'name', 'last_name',),
        }),
        ("Предмет та номер кабінету", {
            'fields': ('name_subject', 'default_room',),
        }),
    )
    # Search Form
    search_fields = ['name_subject', 'default_room', 'name', 'surname', 'last_name']
    # Print list objects
    list_display = ('surname','name','last_name','name_subject', 'default_room')



admin.site.register(Group)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Monday)
admin.site.register(Tuesday)
admin.site.register(Wednesday)
admin.site.register(Thursday)
admin.site.register(Friday)


from django.shortcuts import render
from .models import Post, Group, Teacher, Monday, Tuesday, Wednesday, Thursday, Friday
from django.shortcuts import get_object_or_404
from django.views.generic import View
from .utils import ObjectDetailMixin


class SubjectIndex(View):
    def get(self, request):
        menu = Group.objects.all()
        return render(request, 'timetable/index_timetable.html', context={
            "menu": menu,
        })


class SubjectDetail(View):
    def get(self, request, slug):
        # obj = monday =)
        obj = get_object_or_404(Monday, group__number=slug)
        tuesday = get_object_or_404(Tuesday, group__number=slug)
        wednesday = get_object_or_404(Wednesday, group__number=slug)
        thursday = get_object_or_404(Thursday, group__number=slug)
        friday = get_object_or_404(Friday, group__number=slug)
        menu = Group.objects.all()
        return render(request, 'timetable/detail_timetable.html', context={
            "obj": obj,
            "tuesday": tuesday,
            "wednesday": wednesday,
            "thursday": thursday,
            "friday": friday,
            "menu": menu,
        })



def index(request):
    posts = Post.objects.all()
    return render(request, 'timetable/index.html', {'posts': posts})


class PostDetail(ObjectDetailMixin, View):
    model = Post
    template = 'timetable/detail_group.html'
    queryType = "post"

#class PostDetail(View):
#    def get(self, request, slug    ):
#        post = get_object_or_404(Post, slug__iexact=slug)
#        #post = Post.objects.get(slug__iexact=slug)
#        return render(request, 'timetable/detail_group.html', context={'post': post})


class TagDetail(ObjectDetailMixin, View):
    model = Post
    template = 'timetable/tags.html'
    queryType = "tag"


#def tags(request, slug):
#    post = Post.objects.all().filter(tags__slug__iexact=slug)
#    return render(request, 'timetable/tags.html', context={'tag_posts': post})
